<?php

namespace App\Http\Controllers;

use App\Models\Form;
use App\Mail\AdminMail;
use App\Mail\ContactMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests\PescheriaRequest;
use App\Http\Controllers\PublicController;

class PublicController extends Controller
{
    public function homepage(){
        return view("welcome");
    }

    public function about(){
        return view("about");
    }

    public function gallery(){
        return view("gallery");
    }

    public function service(){
        return view("service");
    }

    public function where(){
        return view("where");
    }

    public function contact(){
        return view("contact");
    }

    public function formSubmit(PescheriaRequest $request){
        $form = Form::create([
            'name'=>$request->input('name'),
            'email'=>$request->input('email'),
            'description'=>$request->input('description'),
        ]);
        $forAdmin = ['name'=>$request->input('name'), 'description'=>$request->input('description'), 'email'=>$request->input('email')];
        $contact = ['name'=>$request->input('name'), 'description'=>$request->input('description')]; 
        Mail::to($request->input('email'))->send(new AdminMail($forAdmin));
        Mail::to($request)->send(new ContactMail($contact));
        
        return redirect(route('homepage'));
}
}
