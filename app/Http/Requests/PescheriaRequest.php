<?php

namespace App\Http\Requests;

use App\Http\Requests\PescheriaRequest;
use Illuminate\Foundation\Http\FormRequest;

class PescheriaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required',
            'email'=>'required',
            'description'=>'required',
        ];
    }

    public function messages(){
        return [
            'name.required'=>'Inserire un nome corretto',
            'email.required'=>'Inserire un email valida',
            'description.required'=>'inserire un messaggio'
        ];
    }
}
