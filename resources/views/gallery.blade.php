<x-layout>
    <section class="animate__animated animate__backInRight my-3 py-3" id="gallery">
        <div class="container">
          <h2 class="text-center text-lg-center mt-4 mb-5"> Gallery</h2>
          <hr class="mt-2 mb-5">
          <div class="row text-center text-lg-start">
            <div class="col-lg-3 col-md-4 col-6">
              <a data-fancybox="gallery" href="{{Storage::url('img/imggallery/banco5.jpg')}}" class="d-block mb-4 h-100">
                <img class="img-fluid img-thumbnail" src="{{Storage::url('img/imggallery/banco5.jpg')}}" alt="">
              </a>
            </div>
            <div class="col-lg-3 col-md-4 col-6">
              <a data-fancybox="gallery" href="{{Storage::url('img/imggallery/Banco1.jpeg')}}" class="d-block mb-4 h-100">
                <img class="img-fluid img-thumbnail" src="{{Storage::url('img/imggallery/Banco1.jpeg')}}" alt="">
              </a>
            </div>
            <div class="col-lg-3 col-md-4 col-6">
              <a data-fancybox="gallery" href="{{Storage::url('img/imggallery/Banco1.jpeg')}}" class="d-block mb-4 h-100">
                <img class="img-fluid img-thumbnail" src="{{Storage::url('img/imggallery/Banco1.jpeg')}}" alt="">
              </a>
            </div>
            <div class="col-lg-3 col-md-4 col-6">
              <a data-fancybox="gallery" href="{{Storage::url('img/imggallery/Banco1.jpeg')}}" class="d-block mb-4 h-100">
                <img class="img-fluid img-thumbnail" src="{{Storage::url('img/imggallery/Banco1.jpeg')}}" alt="">
              </a>
            </div>
            <div class="col-lg-3 col-md-4 col-6">
              <a data-fancybox="gallery" href="{{Storage::url('img/imggallery/Banco1.jpeg')}}" class="d-block mb-4 h-100">
                <img class="img-fluid img-thumbnail" src="{{Storage::url('img/imggallery/Banco1.jpeg')}}" alt="">
              </a>
            </div>
            <div class="col-lg-3 col-md-4 col-6">
              <a data-fancybox="gallery" href="{{Storage::url('img/imggallery/Banco1.jpeg')}}" class="d-block mb-4 h-100">
                <img class="img-fluid img-thumbnail" src="{{Storage::url('img/imggallery/Banco1.jpeg')}}" alt="">
              </a>
            </div>
            <div class="col-lg-3 col-md-4 col-6">
              <a data-fancybox="gallery" href="{{Storage::url('img/imggallery/Banco1.jpeg')}}" class="d-block mb-4 h-100">
                <img class="img-fluid img-thumbnail" src="{{Storage::url('img/imggallery/Banco1.jpeg')}}" alt="">
              </a>
            </div>
            <div class="col-lg-3 col-md-4 col-6">
              <a data-fancybox="gallery" href="{{Storage::url('img/imggallery/Banco1.jpeg')}}" class="d-block mb-4 h-100">
                <img class="img-fluid img-thumbnail" src="{{Storage::url('img/imggallery/Banco1.jpeg')}}" alt="">
              </a>
            </div>
            <div class="col-lg-3 col-md-4 col-6">
              <a data-fancybox="gallery" href="{{Storage::url('img/imggallery/Banco1.jpeg')}}" class="d-block mb-4 h-100">
                <img class="img-fluid img-thumbnail" src="{{Storage::url('img/imggallery/Banco1.jpeg')}}" alt="">
              </a>
            </div>
            <div class="col-lg-3 col-md-4 col-6">
              <a data-fancybox="gallery" href="{{Storage::url('img/imggallery/Banco1.jpeg')}}" class="d-block mb-4 h-100">
                <img class="img-fluid img-thumbnail" src="{{Storage::url('img/imggallery/Banco1.jpeg')}}" alt="">
              </a>
            </div>
            <div class="col-lg-3 col-md-4 col-6">
              <a data-fancybox="gallery" href="{{Storage::url('img/imggallery/Banco1.jpeg')}}" class="d-block mb-4 h-100">
                <img class="img-fluid img-thumbnail" src="{{Storage::url('img/imggallery/Banco1.jpeg')}}" alt="">
              </a>
            </div>
            <div class="col-lg-3 col-md-4 col-6">
              <a data-fancybox="gallery" href="{{Storage::url('img/imggallery/Banco1.jpeg')}}" class="d-block mb-4 h-100">
                <img class="img-fluid img-thumbnail" src="{{Storage::url('img/imggallery/Banco1.jpeg')}}" alt="">
              </a>
            </div>
          </div>
        </div>
      </section>
</x-layout>