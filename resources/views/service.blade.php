<x-layout>
    <div class="container animate__animated animate__backInDown my-5 py-5">
        <div class="row mb-5 justify-content-center align-items-center">
          <h4 class="display-3 text-center">I Nostri Servizi</h4>
            <div class="col-12 col-md-4 card-custom py-5">
                <img src="{{Storage::url('img/imgservice/service4.jpg')}}" class="card-img-top" alt="...">
                <div class="card-body">
                  <h4 class="card-title">Prenotazioni in anticipo</h4>
                  <p class="card-text">Per evitare che il prodotto da voi desiderato non sia più disponibile,presso la
                    nostra pescheria è possibile prenotare in anticipo il pesce.</p>
                </div>
            </div>
            <div class="col-12 col-md-4 card-custom py-5">
                <img src="{{Storage::url('img/imgservice/service3.jpg')}}" class="card-img-top" alt="...">
                <div class="card-body">
                  <h4 class="card-title">Pesce fresco dai nostri mari</h4>
                  <p class="card-text">Crostacei e frutti di mare vengono accuratamente selezionati dallo staff della
                    pescheria.</p>
                </div>
            </div>
            <div class="col-12 col-md-4 card-custom py-5">
                <img src="{{Storage::url('img/imgservice/service2.jpg')}}" class="card-img-top" alt="...">
                <div class="card-body">
                  <h4 class="card-title">Confezionati</h4>
                  <p class="card-text">Salati, Affumicati, Marinati, congelati, Essiccati.</p>
                </div>
            </div>
            <div class="col-12 col-md-4 card-custom py-5">
                <img src="{{Storage::url('img/imgservice/banco5.jpg')}}" class="card-img-top" alt="...">
                <div class="card-body">
                  <h4 class="card-title">Ampia varietà</h4>
                  <p class="card-text">Eccellente qualità proveniente dai migliori mari e oceani.</p>
                </div>
            </div>
            <div class="col-12 col-md-4 card-custom py-5">
                <img src="{{Storage::url('img/imgservice/service6.png')}}" class="card-img-top" alt="...">
                <div class="card-body">
                  <h4 class="card-title">Preparazioni</h4>
                  <p class="card-text">Tartare di tonno, salmone, branzino, gamberi, Sashimi e carpaccio di pesce e tanti
                    altri.</p>
                </div>
            </div>
            <div class="col-12 col-md-4 card-custom py-5">
                <img class="w-75" src="{{Storage::url('img/imgservice/service1.jpg')}}" class="card-img-top" alt="...">
                <div class="card-body">
                  <h4 class="card-title">Pulitura e Sfilettatura</h4>
                  <p class="card-text">Eviscerazione, Filettatura e pulizia di tutti i tipi di pesce.</p>
                </div>
            </div>
            <div class="button-info text-center">
             <a href="tel:+393892569858" class="btn btn-dark w-25 link-info my-3" >Chiamaci</a>
             <a href="{{route("contatti")}}" class="btn btn-dark link-info w-25 my-3" >Contattaci</a>
            </div>
        </div>
    </div>
</x-layout>