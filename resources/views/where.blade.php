<x-layout>
 <div class="container my-5 py-5 animate__animated animate__fadeInLeft">
   <div class="row justify-content-center align-items-center text-center">
     <div class="col-12 col-md-6">
      <img class="w-75" src="{{Storage::url('img/pesci7.jpg')}}" alt="...">
     </div>
     <div class="col-12 col-md-6 py-3">
      <h5 class="display-6"> Mercoledì:</h5>
      <p class="lead fs-4">Via Monte Grappa, Rovellasca (CO)</p>
      <a href="https://maps.app.goo.gl/RrB5kdVtjaAefqM69" class="btn btn-dark" class="link-info">Vieni a
        Trovarci</a>
     </div>
   </div>
 </div>
 <div class="container my-5 py-5 animate__animated animate__fadeInRight">
  <div class="row justify-content-center align-items-center text-center">
    <div class="col-12 col-md-6 py-3">
      <h5 class="display-6"> Giovedì:</h5>
      <p class="lead fs-4">Piazza Della Pace, Cesate (MI)</p>
      <a href="https://maps.app.goo.gl/b22vyqXzhSWbWmeU7" class="btn btn-dark" class="link-info">Vieni a
        Trovarci</a>
    </div>
    <div class="col-12 col-md-6">
      <img class="w-75" src="{{Storage::url('img/pesci7.jpg')}}" alt="...">
    </div>
  </div>
</div>
<div class="container my-5 py-5 animate__animated animate__fadeInLeft">
  <div class="row justify-content-center align-items-center text-center">
    <div class="col-12 col-md-6 py-3">
      <img class="w-75" src="{{Storage::url('img/pesci7.jpg')}}" alt="...">
    </div>
    <div class="col-12 col-md-6">
      <h5 class="display-6"> Venerdì:</h5>
      <p class="lead fs-4">Piazza Mercato, Garbagnate Milanese (MI)</p>
      <a href="https://maps.app.goo.gl/DcMdwg1HBBqNhAwR7" class="btn btn-dark" class="link-info">Vieni a
        Trovarci</a>
    </div>
  </div>
</div>
<div class="container my-5 py-5 animate__animated animate__fadeInRight">
  <div class="row justify-content-center align-items-center text-center">
    <div class="col-12 col-md-6 py-3">
      <h5 class="display-6"> Sabato:</h5>
      <p class="lead fs-4">Via Barbaiana, Lainate (MI)</p>
      <a href="https://maps.app.goo.gl/GFAWMK9RTSDrCK5u7" class="btn btn-dark" class="link-info">Vieni a
        Trovarci</a>
    </div>
    <div class="col-12 col-md-6">
      <img class="w-75" src="{{Storage::url('img/pesci7.jpg')}}" alt="...">
    </div>
  </div>
</div>
</x-layout>