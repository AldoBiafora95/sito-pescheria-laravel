<x-layout>
  <!-- Section 1 -->
  <section class="animate__animated animate__jackInTheBox" id="showcase">
    <div class="container">
      <h1>Odora il Mare </h1>
      <p>Specialità ittiche fresche</p>
      <a id="btn1" class="btn btn-dark" href="{{route("servizi")}}">I Nostri Servizi</a>
    </div>
  </section>
  <!-- Section 2 -->
  <section id="showcase-2">
    <div class="container">
      <div class="row justify-content-center align-items-center">
        <div class="col-12 col-md-6 text-center">
          <h3 class="display-5">La storia del nostro pesce</h3>
          <p class="lead">
            L'intera filiera – dall'acquisto del pesce alla lavorazione, fino alla distribuzione – è garantita e
            monitorata nel rispetto di rigorose pratiche,
            di severi standard e capitolati che assicurano qualità e sicurezza.
          </p>
          <a class="btn btn-dark w-50 mb-5" href="{{route("chisiamo")}}">Leggi di più</a>
        </div>
        <div class="col-12 col-md-6">
          <img class="img-fluid" src="{{Storage::url('img/Banco1.jpeg')}}" alt="">
        </div>
      </div>
    </div>
  </section>
  <!-- Section 3 -->
  <section>
    <div class="container">
      <div class="row justify-content-center align-items-center">
        <div class="col-12 col-md-4 col-lg-4 py-4  box-white">
          <div class="text-center">
            <svg xmlns="http://www.w3.org/2000/svg" width="64" height="64" fill="currentColor" class="bi bi-truck mb-4" viewBox="0 0 16 16">
              <path d="M0 3.5A1.5 1.5 0 0 1 1.5 2h9A1.5 1.5 0 0 1 12 3.5V5h1.02a1.5 1.5 0 0 1 1.17.563l1.481 1.85a1.5 1.5 0 0 1 .329.938V10.5a1.5 1.5 0 0 1-1.5 1.5H14a2 2 0 1 1-4 0H5a2 2 0 1 1-3.998-.085A1.5 1.5 0 0 1 0 10.5v-7zm1.294 7.456A1.999 1.999 0 0 1 4.732 11h5.536a2.01 2.01 0 0 1 .732-.732V3.5a.5.5 0 0 0-.5-.5h-9a.5.5 0 0 0-.5.5v7a.5.5 0 0 0 .294.456zM12 10a2 2 0 0 1 1.732 1h.768a.5.5 0 0 0 .5-.5V8.35a.5.5 0 0 0-.11-.312l-1.48-1.85A.5.5 0 0 0 13.02 6H12v4zm-9 1a1 1 0 1 0 0 2 1 1 0 0 0 0-2zm9 0a1 1 0 1 0 0 2 1 1 0 0 0 0-2z"/>
            </svg>
            <h3 class="display-6">Consegna a domicilio</h3>
            <p class="lead">
              Arrivi giornalieri sui nostri banchi di pesce fresco di frutti di mare
              <br>molluschi ancora freschi di pesca.</br>
              Personale altamente qualificato a vostra disposizione per consigliarvi e rispondere alle vostre esigenze.
            </p>
          </div>
        </div>
        <div class="col-12 col-md-4 col-lg-4 py-4  box-dodgerblue">
          <div class="text-center">
            <svg xmlns="http://www.w3.org/2000/svg" width="64" height="64" fill="currentColor" class="bi bi-chat-text mb-4" viewBox="0 0 16 16">
              <path d="M2.678 11.894a1 1 0 0 1 .287.801 10.97 10.97 0 0 1-.398 2c1.395-.323 2.247-.697 2.634-.893a1 1 0 0 1 .71-.074A8.06 8.06 0 0 0 8 14c3.996 0 7-2.807 7-6 0-3.192-3.004-6-7-6S1 4.808 1 8c0 1.468.617 2.83 1.678 3.894zm-.493 3.905a21.682 21.682 0 0 1-.713.129c-.2.032-.352-.176-.273-.362a9.68 9.68 0 0 0 .244-.637l.003-.01c.248-.72.45-1.548.524-2.319C.743 11.37 0 9.76 0 8c0-3.866 3.582-7 8-7s8 3.134 8 7-3.582 7-8 7a9.06 9.06 0 0 1-2.347-.306c-.52.263-1.639.742-3.468 1.105z"/>
              <path d="M4 5.5a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7a.5.5 0 0 1-.5-.5zM4 8a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7A.5.5 0 0 1 4 8zm0 2.5a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 0 1h-4a.5.5 0 0 1-.5-.5z"/>
            </svg>
            <h3 class="display-6">Consigli per ricette </h3>
            <p class="lead">
              Il personale della pescheria è sempre disponibile a consigliarti il condimento
              giusto per esaltare il sapore del pesce appena acquistato,
              indicandoti ricette e metodi di cottura ottimale.
            </p class="lead">
          </div>
        </div>
        <div class="col-12 col-md-4 col-lg-4 py-4  box-white">
          <div class="text-center">
            <svg xmlns="http://www.w3.org/2000/svg" width="64" height="64" fill="currentColor" class="bi bi-stars mb-4" viewBox="0 0 16 16">
              <path d="M7.657 6.247c.11-.33.576-.33.686 0l.645 1.937a2.89 2.89 0 0 0 1.829 1.828l1.936.645c.33.11.33.576 0 .686l-1.937.645a2.89 2.89 0 0 0-1.828 1.829l-.645 1.936a.361.361 0 0 1-.686 0l-.645-1.937a2.89 2.89 0 0 0-1.828-1.828l-1.937-.645a.361.361 0 0 1 0-.686l1.937-.645a2.89 2.89 0 0 0 1.828-1.828l.645-1.937zM3.794 1.148a.217.217 0 0 1 .412 0l.387 1.162c.173.518.579.924 1.097 1.097l1.162.387a.217.217 0 0 1 0 .412l-1.162.387A1.734 1.734 0 0 0 4.593 5.69l-.387 1.162a.217.217 0 0 1-.412 0L3.407 5.69A1.734 1.734 0 0 0 2.31 4.593l-1.162-.387a.217.217 0 0 1 0-.412l1.162-.387A1.734 1.734 0 0 0 3.407 2.31l.387-1.162zM10.863.099a.145.145 0 0 1 .274 0l.258.774c.115.346.386.617.732.732l.774.258a.145.145 0 0 1 0 .274l-.774.258a1.156 1.156 0 0 0-.732.732l-.258.774a.145.145 0 0 1-.274 0l-.258-.774a1.156 1.156 0 0 0-.732-.732L9.1 2.137a.145.145 0 0 1 0-.274l.774-.258c.346-.115.617-.386.732-.732L10.863.1z"/>
            </svg>
            <h3 class="display-6">Cortesia e Prodotti di qualità</h3>
            <p>
              oltre all'ottima qualità dei prodotti, troverai sempre un ambiente familiare e accogliente e la massima cortesia
              da parte dello staff.
              <br>sempre al servizio del cliente per proporre le migliori soluzioni per ogni esigenza.</br>
            </p>
          </div>
        </div>
      </div>
    </div>
  </section>
</x-layout>