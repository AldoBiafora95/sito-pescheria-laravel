<nav class="navbar sticky-top navbar-expand-lg navbar-dark bg-dark">
    <div class="container-fluid">
      <a class="navbar-brand" href="{{route("homepage")}}">Pescheria da Gigi
      </a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto mb-2 mb-lg-0">
          <li class="nav-item">
            <a class="nav-link" aria-current="page" href="{{route("homepage")}}">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{route("chisiamo")}}">Chi siamo</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{route("galleria")}}">Gallery</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{route("servizi")}}">Servizi</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{route("contatti")}}">Contattaci</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{route("where")}}">Dove trovarci</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
