<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com/" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Open+Sans&display=swap" rel="stylesheet">

    <style>

        *{
            font-family: 'Open Sans', sans-serif;
        }
        .btn-custom {
            background-color: red;
            padding: 10px 20px;
            border: none;
            color: white;
            font-weight: bold;
            width: 100%;
        }


        .img-centered {
            text-align: center;
        }
    </style>


</head>
<body class="bg-dark">
        <div class="container w-50 p-3 rounded">
            <div class="row">
                <div class="col-12">
                    <hr class="text-danger fw-bold">
                   <div class="pt-3">
                       <h3 class="text-danger"><em>Dear {{$forAdmin['name']}}</em></h3>
                       <h5>La ringraziamo per averci contattato, questa è stata la sua richiesta:</h5>
                       <p class="lead">{{$forAdmin['email']}}</p>
                       <p class="lead">{{$forAdmin['description']}}
                       </p>
                       <h5 class="text-white">Team Netflix</h5>
                       <button class="btn-custom">Rispondi Subito</button>
                   </div>
                </div>
            </div>
        </div>
</body>
</html>