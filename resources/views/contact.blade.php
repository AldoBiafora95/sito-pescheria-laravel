<x-layout>
  @if ($errors->any())
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
    @endif
    <div class="container mb-5">
        <form class="animate__animated animate__backInLeft" action="{{route('submit')}}" method="POST">
          @csrf
          <div id="form" class="col-lg-8">
            <h2><u>Contattaci</u></h2>
            <label for="exampleFormControlname" class="form-label">Nome e Cognome</label>
            <input name="name" type="text" class="form-control" id="exampleFormControlname" placeholder="Jonh Smith">
            <label for="exampleFormControlemail" class="form-label">Indirizzo Email</label>
            <input name="email" type="email" class="form-control" id="exampleFormControlemail" placeholder="name@example.com">
            <label for="exampleFormControlTextarea1" class="form-label">Messaggio</label>
            <textarea name="description" class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
            <input type="submit" class="btn btn-dark mt-3">
          </div>
        </form>
      </div>
</x-layout>