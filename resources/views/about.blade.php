<x-layout>
    <section class="animate__animated animate__bounceInUp">
        <div class="container my-5 py-5">
          <div class="row justify-content-center align-items-center">
            <div class="col-12 col-md-6">
              <h2 class="display-4">La Nostra Storia</h2>
              <p>
                Gigi segue personalmente i propri clienti, avendo cura di soddisfare ogni esigenza, per un settore come quello
                della ristorazione milanese che ha conosciuto negli ultimi anni un grande sviluppo in termini di qualità e
                ricercatezza
                aiutato dai suoi collaboratori, cura tutte le fasi del lavoro garantendo ai ristoratori di Milano e provincia,
                tra cui spiccano nomi eccellenti, forniture puntuali e di ottima qualità al loro domicilio.
              </p>
              <p>
                Ad assicurare un accurato servizio di pulizia, nonché a proporre cruditè già pronte da mettere in tavola.
                E per soddisfare ancora di più le esigenze degli amanti dei fornelli.
                <br>Qualità, professionalità, passione e dedizione alla propria clientela hanno fatto di Pescheria da Gigi un
                punto di riferimento sicuro per quanti, per lavoro o per piacere,
                desiderano mettere in tavola il fresco sapore del mare.</br>
              </p>
            </div>
            <div class="col-12 col-md-6 text-center">
              <img class="img-about img-fluid" src="{{Storage::url('img/Spada2.jpeg')}}" alt="pesci">
            </div>
          </div>
        </div>
      </section>
      <!-- Section Video -->
      <section id="video-info" class="animate__animated animate__bounceInUp">
        <div class="vidwrapper" data-aos="flip-up" data-aos-duration="3000">
          <div class="vidcontainer">
            <video src="{{Storage::url('img/videopsc2.mp4')}}" controls></video>
          </div>
        </div>
      </section>
</x-layout>






