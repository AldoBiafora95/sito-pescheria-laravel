<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PublicController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PublicController::class, 'homepage'])->name('homepage');
Route::get('/ChiSiamo', [PublicController::class, 'about'])->name('chisiamo');
Route::get('/Galleria', [PublicController::class, 'gallery'])->name('galleria');
Route::get('/Servizi', [PublicController::class, 'service'])->name('servizi');
Route::get('/Dove-Trovarci', [PublicController::class, 'where'])->name('where');
Route::get('/Contattaci', [PublicController::class, 'contact'])->name('contatti');
Route::post('/Contattaci/submit', [PublicController::class, 'formSubmit'])->name('submit');

